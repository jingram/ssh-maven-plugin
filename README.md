# SSH Exec Maven Plugin

The plugin provides a compatible Exec Maven Plugin goal for remote execution of a Java process on a remote host via SSH (Secure Socket Shell).

The plugin supports attaching a debugger to the remote hosts Java process allowing debug integration for variouse IDE's

## Goals Overview

The ssh exec plugin has a single goal

- exec executes a Java program in a seperate process on a remote host after uploading the required artifacts from the maven build. Optionally the goal is able to attach a debugger.

## Usage

Instructions on how to use the SSH Exec Maven Plugin can be found in the [exec goal](#execgoal) section.

If you feel like the plugin is missing a feature or has a defect, you can fill a feature request or bug report using the projects [issue tracker](https://bitbucket.org/jingram/ssh-maven-plugin/issues). 

When creating a new issue, please provide a comprehensive description of your concern. For fixing bugs it is crucial that the developers can reproduce your problem. For this reason please provide entire debug logs, POMs or most preferably a small demo project attached to assist in reproducing the issue.

### System Requirements
Minimum requirements to run this Maven plugin:

**Maven:**  2.2.1

**JDK / JRE:** 1.7

**Memory:** No minimum requirement.

**Disk Space:** No minimum requirement.

### Configuring a Remote Host
#### Linux Host
Configuring a Linux host, including a Raspberry Pi, can be achieved in a few easy steps:

* Install the ssh daemon (sshd) on the remote host
* Configure a user on the host to allow remote password based ssh access

  Key exchange authentication will be supported in a future release
* Test ssh access using the username and password from the command line
* Install a version of the Java Runtime >= 1.7 on the remote host
* Optionally add the java executable to the PATH of the remote user

*You may need to execute the remote Java process using sudo if wanting to use hardware devices such as the I2C bus on the Raspberry Pi* 


### Exec Goal

Using a standard Maven POM, add the SSH Exec plugin

``` xml
   <build>
      <plugins>
         <plugin>
            <groupId>org.rogueware.mojo</groupId>
            <artifactId>ssh-exec-maven-plugin</artifactId>
            <version>1.2</version>
            <configuration>
               <sshHost>10.0.0.1</sshHost>
               <sshUsername>pi</sshUsername>
               <sshPassword>raspberry</sshPassword>
               
               <sudo>false</sudo>
            </configuration>
         </plugin>
      </plugins>
   </build>

```

The plugin can be invoked by running the exec goal
```
mvn org.rogueware.mojo:ssh-exec-maven-plugin:1.2:exec
```

**Plugin Configuration: SSH**

```
sshHost		String		exec.remote.ssh.host
						Hostname or IP address of the remote host
sshPort		Integer		exec.remote.ssh.port
						Port to connect to remote ssh daemon on host 
						Default: 22
sshUsername	String		exec.remote.ssh.username
						Username to connect to remote host
sshPassword	String		exec.remote.ssh.password
						Password to authenticate user on remote host
```
**Plugin Configuration: Sudo**

```
sudo		 Boolean	exec.remote.ssh.sudo
					 	Set to true to enable executing as sudo on remote host
sudoPassword String		Password to use for sudo
						If left out, sshPassword will be used				
```
**Plugin Configuration: Execution Environment**

```
execArgs			String	exec.args
							Exec compatible arguments separated by spaces for the executed Java process. 
							%classpath will be subsituted with classpath from project dependencies 
							Example: -j 20 -Dprop=val -classpath %classpath 
						
classpathScope		String	exec.classpathScope
							Defines the scope of the classpath for uploaded dependencies
	 						One of Maven defined compile, test, runtime, system 
	 						Default: runtime
workingDirectory	String	exec.workingdir
							The remote working directory. 
							Can be relative to user home directory using .
							Default: ./ssh-exec/{project dir name}
remoteExecutable	String	exec.remote.executable
							Specifies the remote executable to execute the Java process
							Default: java
```	

**Classpath**

The plugin will substitute the *%classpath* in the exec.args with the correct dependencies that have been uploaded to the remote host derived from the maven project dependencies in the specified classpath scope.

The default maven classpath scope is runtime.

**Debug**

The plugin will automatically detect the -Xrunjdwp parameter if defined in the execArgs config / exec.args property when setting up debugging for the executed Java process.

Example: -Xdebug -Xrunjdwp:transport=dt_socket,server=n,address=127.0.0.1

The plugin establishes port forward / reverse tunneling based on the server=[y/n] for the debugger to attach to the Java process on the remote host.

*The plugin only works with the transport=dt_socket and understands both ip/host and port in the address format address=[name:port]*


## Examples

### Run Maven project
Run the Maven project artifact on remote host using runtime dependencies
```
mvn "-Dexec.args=-classpath %classpath org.rogueware.prototype.usemojo.Main" org.rogueware.mojo:ssh-exec-maven-plugin:1.2:exec
```

### Run Maven project local debugger
Run the Maven project artifact on remote host using runtime dependencies with remote processes connecting back to local debugger
```
mvn "-Dexec.args=-Xdebug -Xrunjdwp:transport=dt_socket,server=n,address=61118 -classpath %classpath org.rogueware.prototype.usemojo.Main" -Djpda.listen=true -Djpda.address=61118 org.rogueware.mojo:ssh-exec-maven-plugin:1.2:exec
```

### Run Maven project remote debugger
Run the Maven project artifact on remote host using runtime dependencies with remote processes 
```
mvn "-Dexec.args=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=20345 -classpath %classpath org.rogueware.prototype.usemojo.Main" org.rogueware.mojo:ssh-exec-maven-plugin:1.2:exec
```

## IDE Integration

### Netbeans

To allow Netbeans to execute and or remote debug a standard Maven project on a remote host via SSH:

1. Open the Maven project in Netbeans
2. Configure the project POM by adding the SSH Exec plugin and configuration under the \<plugins\> section
3. Change the "Run project" and "Debug project" actions in Netbeans

- Right click on the Maven project in the Projects tab
- Select Properties
- Click on Actions
- Select "Run project" in the actions list
- Replace "org.codehaus.mojo:exec-maven-plugin:1.2.1:exec" with "org.rogueware.mojo:ssh-exec-maven-plugin:1.2:exec" in the Execute Goals 
- Select "Debug project" in the actions list
- Replace "org.codehaus.mojo:exec-maven-plugin:1.2.1:exec" with "org.rogueware.mojo:ssh-exec-maven-plugin:1.2:exec" in the Execute Goals 

*Note: Versions of org.codehaus.mojo:exec-maven-plugin and org.rogueware.mojo:ssh-exec-maven-plugin may differ*

You will now be able to run and remote debug you Maven project as a remote Java process on the host defined in the plugin configuration of your POM file. 
