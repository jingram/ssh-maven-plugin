/*
 * TSshSession.java
 * 
 * Copyright 2016 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package ssh;

import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.IOException;
import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;
import org.rogueware.mojo.SshSession;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TSshSession {

   String host = "10.0.0.2";
   int port = 22;
   String usr = "pi";
   String password = "112233";

   @Test
   public void testSFTP() throws MojoExecutionException, IOException, SftpException {
      //SshSession s = new SshSession();

      try (SshSession s = new SshSession(null)) {
         s.connect(host, port, usr, password);

         // Absolute working dir
         File f1 = new File("target/classes/org/rogueware/mojo/FlyExecMojo.class");
         s.uploadFile("/home/pi/gogo/myApp", f1, "classes");

         // Relative working dir
         File f2 = new File("target/classes/org/rogueware/mojo/SshSession.class");
         s.uploadFile("gogo/myApp", f2, "classes");

         // Relative working dir overwrite
         s.uploadFile("./gogo/myApp", f2, "classes");

         s.establishReverseTunnel(5000, 3306);

         int code = s.exec("ls -l", System.out, System.err, null, null);
         //code = s.exec("sleep 20; echo \"Done!\"", System.out, System.err, null);
         code = s.exec("ls broken", System.out, System.err, null, null);
         code = s.exec("echo 'Hello stderr' 1>&2", null, System.err, null, null);
         code = s.exec("cd /home; pwd", System.out, System.err, null, null);
         code = s.exec("pwd", System.out, System.err, null, null);
      }
   }
}
