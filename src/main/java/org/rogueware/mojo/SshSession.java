/*
 * SshSession.java
 * 
 * Defines a class used to encode and decode memory mappings
 * 
 * Copyright 2016 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.mojo;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class SshSession implements Closeable {

   private JSch jsch;
   private Session session;

   private ChannelSftp channelSftp;
   private String remoteHomeDir;

   private Integer listenRemotePort;
   private Integer listenLocalPort;

   private String hostOrIp;
   private int port;
   private String username;

   private final Log log;

   public SshSession(Log log) {
      this.log = log;
   }

   //TODO: Connect with private key public void connect(String hostOrIp, int port, String username, key or path to key, passphrase (prompt or config) 
   public synchronized void connect(String hostOrIp, int port, String username, String password) throws MojoExecutionException {
      this.hostOrIp = hostOrIp;
      this.username = username;
      this.port = port;

      // Create a session using password authentication
      try {
         jsch = new JSch();
         session = jsch.getSession(username, hostOrIp, port);
         session.setConfig("StrictHostKeyChecking", "no");
         session.setPassword(password);

         session.connect(15000);  // 15s timeout         
         session.setServerAliveInterval(15000);  // Keep alive every 15s
         
         if (!session.isConnected()) {
            throw new MojoExecutionException(String.format("Unable to establish ssh connnec to '%s@%s' port '%d'", username, hostOrIp, port));
         }
      } catch (JSchException ex) {
         throw new MojoExecutionException(ex.getMessage(), ex);
      }
   }

   public String getRemotePath(String workingDirectory, String relativePath) throws MojoExecutionException {
      // Open SFTP Channel if not already open
      connectSFtpChannel();

      String destinationDirectory;
      if (!workingDirectory.startsWith("/")) {
         // Relative to the initial users directory
         destinationDirectory = remoteHomeDir + "/" + workingDirectory.replace("./", "") + "/" + relativePath;
      } else {
         // Absolute directory
         destinationDirectory = workingDirectory + "/" + relativePath;
      }
      destinationDirectory = destinationDirectory.replaceAll("//", "/");
      return destinationDirectory;
   }

   // TODO: Have file with MD5 on server, download and only upload file if hash does not match local .. save time
   public synchronized boolean uploadFile(String workingDirectory, File localFile, String relativeUploadDirectory) throws MojoExecutionException {
      if (!localFile.isFile()) {
         throw new MojoExecutionException(String.format("Dependent file '%s' is not a file", localFile.getAbsolutePath()));
      }
      if (!localFile.canRead()) {
         throw new MojoExecutionException(String.format("Dependent file '%s' is not readable", localFile.getAbsolutePath()));
      }

      // Open SFTP Channel if not already open
      connectSFtpChannel();

      // Upload each file overwriting
      boolean uploaded = false;
      try {
         String destinationDirectory = getRemotePath(workingDirectory, relativeUploadDirectory);
         String destinationFile = destinationDirectory + "/" + localFile.getName();

         // Change to file destination dir and upload the file
         changeAndOrCreateDir(channelSftp, destinationDirectory);

         channelSftp.put(new FileInputStream(localFile), destinationFile, ChannelSftp.OVERWRITE);
         uploaded = true;

         // Change back to original working dir
         channelSftp.cd(remoteHomeDir);
      } catch (FileNotFoundException | SftpException ex) {
         throw new MojoExecutionException(ex.getMessage(), ex);
      }

      return uploaded;
   }

   public void deleteDirectory(String dir) throws MojoExecutionException {
      // Open SFTP Channel if not already open
      connectSFtpChannel();

      // TODO: Test if we need to recursivly delete and recursively delete
      //channelSftp.r
   }

   public void establishReverseTunnel(int listenRemotePort, int forwardLocalPort) throws MojoExecutionException {
      try {
         session.setPortForwardingR(listenRemotePort, "localhost", forwardLocalPort);
         this.listenRemotePort = listenRemotePort;
      } catch (JSchException ex) {
         throw new MojoExecutionException(ex.getMessage(), ex);
      }
   }

   public void establishTunnel(int listenLocalPort, int forwardRemotePort) throws MojoExecutionException {
      try {
         session.setPortForwardingL(listenLocalPort, "localhost", forwardRemotePort);
         this.listenLocalPort = listenLocalPort;
      } catch (JSchException ex) {
         throw new MojoExecutionException(ex.getMessage(), ex);
      }
   }

   // http://www.programcreek.com/java-api-examples/index.php?api=com.jcraft.jsch.ChannelExec
   // TODO: try and get X11 working with running a gui app on pi
   public int exec(String command, OutputStream out, OutputStream err, InputStream in, String sudoPassword) throws MojoExecutionException {

      ChannelExec channel = null;
      try {
         channel = (ChannelExec) session.openChannel("exec");

         channel.setInputStream(in, true);
         channel.setErrStream(err, true);

         OutputStream sudoPassOut = null;
         if (null == sudoPassword) {
            channel.setOutputStream(out, true);
         } else {
            sudoPassOut = channel.getOutputStream();
         }

         // Execute command
         channel.setCommand(command);
         channel.connect(15000);

         // Send sudo password if required and then switch streams
         if (null != sudoPassOut) {
            sudoPassOut.write((sudoPassword + "\n").getBytes());
            sudoPassOut.flush();
            if (null != log) {
               log.info("Sent sudo password");
            }

            channel.setOutputStream(out, true);
         }
         if (null != log) {
            log.info("------------------------------------------------------------------------\n");
         }

         // Poll streams
         while (true) {
            if (channel.isClosed()) {
               break;
            }

            Thread.sleep(25);
         }

         return channel.getExitStatus();
      } catch (JSchException | InterruptedException | IOException ex) {
         throw new MojoExecutionException(ex.getMessage(), ex);
      } finally {
         if (null != channel) {
            channel.setInputStream(null);
            channel.setOutputStream(null);
            channel.setErrStream(null);
            channel.disconnect();
         }
      }
   }

   @Override
   public synchronized void close() throws IOException {
      if (null != channelSftp) {
         channelSftp.exit();
         channelSftp.disconnect();
         channelSftp = null;
      }

      if (null != session) {
         if (null != listenRemotePort) {
            try {
               session.delPortForwardingR(listenRemotePort);
            } catch (JSchException exIgnore) {
            }
         }

         if (null != listenLocalPort) {
            try {
               session.delPortForwardingL(listenLocalPort);
            } catch (JSchException exIgnore) {
            }
         }

         session.disconnect();
         session = null;
      }

      if (null != jsch) {
         jsch = null;
      }
   }

   private void connectSFtpChannel() throws MojoExecutionException {
      if (null == channelSftp) {
         try {
            channelSftp = (ChannelSftp) session.openChannel("sftp");
            // logger.info("channel connect begin");
            channelSftp.connect();
            remoteHomeDir = channelSftp.pwd();
         } catch (JSchException | SftpException ex) {
            throw new MojoExecutionException(ex.getMessage(), ex);
         }
      }
   }

   private void changeAndOrCreateDir(ChannelSftp channel, String dir) throws MojoExecutionException {
      // Recursive function to create missing directories
      try {
         channel.cd(dir);
      } catch (SftpException ex) {
         try {
            channel.mkdir(dir);
            channel.cd(dir);
         } catch (SftpException ex2) {
            String dotDot = dir.substring(0, dir.lastIndexOf('/'));
            if (dotDot.isEmpty()) {
               return;
            }
            changeAndOrCreateDir(channel, dotDot);
            changeAndOrCreateDir(channel, dir);    // Step forward again
         }
      }
   }
}
