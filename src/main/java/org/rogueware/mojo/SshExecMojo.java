/*
 * SshExecMojo.java
 * 
 * Defines a class used to encode and decode memory mappings
 * 
 * Copyright 2016 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.mojo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Mojo(name = "exec", requiresDependencyResolution = ResolutionScope.TEST)
public class SshExecMojo extends AbstractMojo {

   /**
    * The project.
    */
   @Parameter(defaultValue = "${project}", readonly = true)
   private MavenProject project;

   /**
    * The base directory of the project.
    */
   @Parameter(readonly = true, required = true, defaultValue = "${basedir}")
   private File basedir;

   /**
    * Exec Maven Plugin - Compatible Arguments separated by space for the
    * executed program. For example: "-j 20"
    */
   @Parameter(property = "exec.args")
   private String execArgs;

   /**
    * Exec Maven Plugin - Defines the scope of the classpath passed to the
    * plugin. Set to compile,test,runtime or system depending on your needs.
    * Default value is 'runtime' instead of 'compile'.
    */
   @Parameter(property = "exec.classpathScope", defaultValue = "runtime")
   private String classpathScope;

   /**
    * Exec Maven Plugin - Compatible Argument The remote working directory.
    * Optional. If not specified, ./ssh-exec/{project dir name}
    */
   @Parameter(property = "exec.workingdir")
   private String workingDirectory;

   /**
    * Exec Maven Plugin Replaces exec.executable for a remote executable to run
    */
   @Parameter(property = "exec.remote.executable", defaultValue = "java")
   private String remoteExecutable;

   /**
    * SSH Settings
    */
   @Parameter(property = "exec.remote.ssh.host")
   private String sshHost;

   @Parameter(property = "exec.remote.ssh.port", defaultValue = "22")
   private String sshPort;

   @Parameter(property = "exec.remote.ssh.username")
   private String sshUsername;

   @Parameter(property = "exec.remote.ssh.password")
   private String sshPassword;

   /**
    * Sudo Settings
    */
   @Parameter(property = "exec.remote.ssh.sudo", defaultValue = "false")
   private boolean sudo;

   @Parameter(property = "exec.remote.ssh.sudo.password")
   private String sudoPassword;

   @Override
   public void execute() throws MojoExecutionException, MojoFailureException {
      // Get the scoped artifacts and output classes etc
      Map<File, String> uploadFiles = new HashMap<>();
      List<String> relativeClassPaths = new ArrayList<>();

      // Working directory on remote host
      String wDir = getWorkingDirectory();

      // Get project dependencies and add to upload files
      getScopedArtifacts(uploadFiles);
      getTargetClasses(uploadFiles, relativeClassPaths);

      // Connect the ssh session
      try (SshSession session = connectToSsh()) {
         // Build class path using directories on remote host pointing to uploaded artifacts and class directories
         String classPath = buildClassPath(session, wDir, relativeClassPaths);

         // If debugging enabled, setup correct port forwarding
         enableDebugging(session);

         // Upload the files
         uploadRequiredFiles(session, wDir, uploadFiles);

         // Execute the commmand on the remote host
         executeCommand(session, wDir, classPath);
      } catch (IOException ex) {
         throw new MojoExecutionException(ex.getMessage(), ex);
      }

   }

   private String getWorkingDirectory() {
      if (null == workingDirectory) {
         return "./ssh-exec/" + basedir.getName();
      } else {
         return workingDirectory;
      }
   }

   private void getScopedArtifacts(Map<File, String> uploadFiles) {
      List<Artifact> artifacts;
      switch (classpathScope) {
         case "compile":
            artifacts = project.getCompileArtifacts();
            break;
         case "test":
            artifacts = project.getTestArtifacts();
            break;
         case "runtime":
            artifacts = project.getRuntimeArtifacts();
            break;
         case "system":
            artifacts = project.getSystemArtifacts();
            break;
         default:
            throw new IllegalStateException("Invalid classpath scope: " + classpathScope);
      }

      // Artifact dependencies will be uploaded to a lib directory
      for (Artifact artifact : artifacts) {
         // Artifact may be another local Maven project pointing to its dependency classes 
         // Need to uploaded its target/classes dir contents to relative path on remote
         if (artifact.getFile().isDirectory()) {
            String[] parts = artifact.getFile().getAbsolutePath().split("target");
            if (2 == parts.length) {
               String removeDir = parts[0] + "target";
               String relativeDir = artifact.getFile().getAbsolutePath().replace(removeDir, "");
               getLog().debug(String.format("Adding dependency target directory '%s' to be uploaded to relative path '%s'", artifact.getFile().getAbsoluteFile(), relativeDir));
               walkDir(artifact.getFile(), uploadFiles, removeDir);
            }
         } else {
            // Normal .m2 repository jar
            uploadFiles.put(artifact.getFile(), "lib");
            getLog().debug(String.format("Adding dependency '%s' to be uploaded to relative path '%s'", artifact.getFile().getAbsoluteFile(), uploadFiles.get(artifact.getFile())));
         }
      }
   }

   private void getTargetClasses(Map<File, String> uploadFiles, List<String> relativeClassPaths) throws MojoExecutionException {
      File outputDir = new File(project.getBuild().getOutputDirectory());
      walkDir(outputDir, uploadFiles);

      // Class path for classes dir
      String relPath = project.getBuild().getOutputDirectory().replace(project.getBuild().getDirectory(), "");
      if (relPath.startsWith("/")) {
         relPath = relPath.substring(1);
      }
      relativeClassPaths.add(relPath);

      // If test scoped, add the test output
      if ("test".equals(classpathScope)) {
         File testOutputDir = new File(project.getBuild().getTestOutputDirectory());
         walkDir(testOutputDir, uploadFiles);

         // Class path for test classes dir
         relPath = project.getBuild().getTestOutputDirectory().replace(project.getBuild().getDirectory(), "");
         if (relPath.startsWith("/")) {
            relPath = relPath.substring(1);
         }
         relativeClassPaths.add(relPath);
      }
   }

   private void walkDir(File dir, Map<File, String> uploadFiles) {
      walkDir(dir, uploadFiles, project.getBuild().getDirectory());
   }

   private void walkDir(File dir, Map<File, String> uploadFiles, String removeDir) {
      File[] files = dir.listFiles();
      for (File f : files) {
         // Recurse walking directories
         if (f.isDirectory()) {
            walkDir(f, uploadFiles, removeDir);
         } else {
            String relPath = f.getParentFile().getAbsolutePath().replace(removeDir, "");
            if (relPath.startsWith(File.pathSeparator)) {
               relPath = relPath.substring(1);
            }

            // Convert windows path to unix
            relPath = relPath.replace("\\", "/");

            uploadFiles.put(f, relPath);
            getLog().debug(String.format("Adding output '%s' to be uploaded to relative path '%s'", f.getAbsoluteFile(), uploadFiles.get(f)));
         }
      }
   }

   private SshSession connectToSsh() throws MojoExecutionException {
      if (null == sshHost || sshHost.isEmpty()) {
         throw new MojoExecutionException("sshHost configuration is required");
      }
      if (null == sshUsername || sshUsername.isEmpty()) {
         throw new MojoExecutionException("sshUsername configuration is required");
      }

      int port;
      try {
         port = Integer.parseInt(sshPort);
      } catch (NumberFormatException ex) {
         throw new MojoExecutionException("sshPort configuration is invalid");
      }

      // TODO: Password, password from file, key, key from file
      // Password
      if (null != sshPassword && !sshPassword.isEmpty()) {
         SshSession session = new SshSession(getLog());
         session.connect(sshHost, port, sshUsername, sshPassword);
         return session;
      } else {
         throw new MojoExecutionException("Unable to determine ssh authentication, please specify appropriate ssh configuration for passwords or keys");
      }
   }

   private void enableDebugging(SshSession session) throws MojoExecutionException {
      // Check if the command line has debuggging parameters and enable the apropriate port forwarding 
      String lowerExecArgs = execArgs.toLowerCase();
      if (lowerExecArgs.contains("-xrunjdwp")) {
         // Debugger requested to be enabled ... is process acting as server or client and what port number
         Map<String, String> jdwpParams = new HashMap<>();

         try {
            int startParamIndex = lowerExecArgs.indexOf("-xrunjdwp:");
            int endParamIndex = lowerExecArgs.indexOf(' ', startParamIndex);
            int endParamCloseIndex = lowerExecArgs.indexOf('"', startParamIndex);

            if (-1 == endParamIndex && endParamCloseIndex != -1 && endParamCloseIndex < endParamCloseIndex) {
               endParamIndex = endParamCloseIndex;
            }
            if (-1 == endParamIndex) {
               endParamIndex = lowerExecArgs.length() - 1;
            }

            String jdwpParamsStr = lowerExecArgs.substring(startParamIndex + 10, endParamIndex);
            for (String param : jdwpParamsStr.split(",")) {
               String[] namVal = param.split("=");
               jdwpParams.put(namVal[0], namVal[1]);
            }
         } catch (Exception ex) {
            throw new MojoExecutionException("Unable to parse -Xrunjdwp parameter", ex);
         }

         int debuggerPort;
         try {
            debuggerPort = Integer.parseInt(jdwpParams.get("address"));
         } catch (NumberFormatException ex) {
            throw new MojoExecutionException(String.format("Unable to parse port only out of -Xrunjdwp parameter address=%s", jdwpParams.get("address")), ex);
         }

         // Port Forward or Remote Port Forward (Depends on if acting as debugger server or not
         if (jdwpParams.containsKey("server")) {
            if ("n".equals(jdwpParams.get("server"))) {
               // Debugger listening on the host, port forward remote port to local host port
               session.establishReverseTunnel(debuggerPort, debuggerPort);
               getLog().info(String.format("Established remote host port forwarding to enable debugging on port %d", debuggerPort));
            } else if ("y".equals(jdwpParams.get("server"))) {
               // Debugger listening on the java app remote, port forward loval port to remote host port
               session.establishTunnel(debuggerPort, debuggerPort);
               getLog().info(String.format("Established port forwarding to enable debugging on port %d", debuggerPort));
            }
         }
      }
   }

   private void uploadRequiredFiles(SshSession session, String wDir, Map<File, String> uploadFiles) throws MojoExecutionException {
      for (File f : uploadFiles.keySet()) {
         String logDest = (wDir + "/" + uploadFiles.get(f)).replace("//", "/") + "/" + f.getName();

         session.uploadFile(wDir, f, uploadFiles.get(f));
         getLog().info(String.format("Uploaded file '%s'", logDest));
      }
   }

   private String buildClassPath(SshSession session, String wDir, List<String> relativeClassPaths) throws MojoExecutionException {
      List< String> classPaths = new ArrayList<>();

      // Class paths for class and test class output directories
      for (String path : relativeClassPaths) {
         classPaths.add(session.getRemotePath(wDir, path));
      }

      // Class paths for each JAR
      classPaths.add(session.getRemotePath(wDir, "lib/*"));

      String classPath = String.join(":", classPaths.toArray(new String[0]));
      getLog().info(String.format("Using remote classpath '%s'", classPath));
      return classPath;
   }

   private void executeCommand(SshSession session, String wDir, String classPath) throws MojoExecutionException {
      String commandCd = "cd " + wDir + ";";  // Change into the working directory 

      String command = remoteExecutable + " "; // Java to execute
      command += execArgs + " ";  // Exection arguments to pass to Java
      command = command.replaceAll("%classpath", classPath);  // Substitute the correct classpath

      // Sudo
      String sendSudoPass = null;
      if (sudo) {
         //   -S  The -S (stdin) option causes sudo to read the password from the standard input instead of the terminal device.
         //   -p  The -p (prompt) option allows you to override the default password prompt and use a custom one.         
         command = "sudo -S -p '' " + command;

         // Make sure password set from either ssh user password or specified sudo password
         sendSudoPass = (null == sudoPassword || sudoPassword.isEmpty()) ? (null == sshPassword || sshPassword.isEmpty()) ? null : sshPassword : sudoPassword;
      }

      getLog().info(commandCd + command);
      getLog().info("------------------------------------------------------------------------");
      int resultCode = session.exec(commandCd + command, System.out, System.err, System.in, sendSudoPass);
      if (0 != resultCode) {
         String message = "Result of " + command + " execution is: '" + resultCode + "'.";
         getLog().error(message);
         throw new MojoExecutionException(message);
      }
   }
}
